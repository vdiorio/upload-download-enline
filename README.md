<!-- ABOUT THE PROJECT -->
## Sobre este projeto

Este aplicativo foi montado para a participação de um processo seletivo, existe a intenção de aprimorá-lo futuramente, e com essa atualização o read-me também será atualizado.

* [Teste a aplicação aqui!](https://vitor-upload.herokuapp.com)


Este aplicativo tem como objetivo:
* Ser uma aplicação para upload e  download de arquivos.
* Salvar arquivos em collections no mongoDB.
* Mostrar arquivos salvos na tela inicial.

<p align="right">(<a href="#top">voltar para o topo</a>)</p>



### Feito com

* [React.js](https://reactjs.org/)
* [Typescript](https://www.typescriptlang.org/)
* [Node.js](https://nodejs.org/)
* [Mocha.js](https://mochajs.org)
* [Chai.js](https://www.chaijs.com)
* [Sinon.js](https://sinonjs.org)
* [Express.js](https://expressjs.com)
* [Sequelize](https://sequelize.org)
* [Docker](https://www.docker.com)
* [MongoDb](https://www.mongodb.com)
* [RTL](https://testing-library.com)

<p align="right">(<a href="#top">voltar para o topo</a>)</p>



<!-- GETTING STARTED -->
## Iniciando

Os próximos passos te ensinarão a montar a aplicação na sua máquina.

### Pré-requisitos

* npm ou yarn
* Docker
* MongoDB


### Instalação

### 1. Clone o repositório
   ```sh
   git clone https://gitlab.com/vdiorio/upload-download-enline
   ```
### 2. Entre na pasta criada
   ```sh
   cd upload-download-enline
   ```
### 3. Instale os pacotes
   ```sh
   npm install
   ```
   OU
   
   ```sh
   yarn install
   ```

### 4. A aplicação está pronta para rodar em containers ou localmente.

##### 4.1 Para rodar localmente rode o comando:
```sh
npm start
```
OU 
```sh
yarn start
```


  A aplicação ativará por padrão o backend na porta 3001 e o frontend na porta 3000 (Há arquivos **.env.example** para alterar as portas caso sinta necessidade).
  Acesse [localhost:3000](https://localhost:3000) para acessar o frontend e [localhost:3001](https://localhost:3001) para acessar o backend.


  ##### 4.2 Para rodar em containers do docker rode o comando:
  
```sh
npm run compose:up
```

OU 

```sh
yarn compose:up
```
  O docker-compose ativará o backend na porta 3001 e o frontend na porta 3000.
  Acesse [localhost:3000](https://localhost:3000) para acessar o frontend e [localhost:3001](https://localhost:3001) para acessar o backend.


<p align="right">(<a href="#top">voltar para o topo</a>)</p>


<!-- CONTACT -->
## Contato

Vitor Diorio - [@Samuraidoaxe](https://twitter.com/Samuraidoaxe) - [Linkedin](https://www.linkedin.com/in/vitordiorio/) - vdioriomd@gmail.com

Project Link: [https://gitlab.com/vdiorio/upload-download-enline](https://gitlab.com/vdiorio/upload-download-enline)

<p align="right">(<a href="#top">voltar para o topo</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Links úteis

Apenas alguns links que sempre me ajudam e que eu gosto de recomendar.

* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Malven's Flexbox Cheatsheet](https://flexbox.malven.co/)
* [Malven's Grid Cheatsheet](https://grid.malven.co/)
* [Img Shields](https://shields.io)
* [GitHub Pages](https://pages.github.com)
* [Font Awesome](https://fontawesome.com)
* [React Icons](https://react-icons.github.io/react-icons/search)

<p align="right">(<a href="#top">voltar para o topo</a>)</p>
