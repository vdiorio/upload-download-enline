import upload from './upload.controller';
import files from './files.controller';
import download from './download.controller';

export default {
  download,
  files,
  upload,
};
