import { StatusCodes } from 'http-status-codes';
import { ObjectId } from 'mongodb';
import { Request, Response } from 'express';
import formatResponse from '../services/response.service';
import DbResponse from '../interfaces/dbResponse';
import fileStream from '../config/fileStream';

const getAllFiles = async (_req: Request, res: Response) => {
  try {
    const gfs = fileStream.gfs();
    const files = await gfs.files.find().toArray();
    const response = formatResponse(files as unknown as DbResponse[]);
    return res.status(StatusCodes.OK).json(response);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Internal server error' });
  }
};

const getFileById = async (req: Request, res: Response) => {
  try {
    const gfs = fileStream.gfs();
    const { id } = req.params;
    const file = await gfs.files.findOne({ _id: new ObjectId(id) });
    if (!file) return res.status(StatusCodes.NOT_FOUND).json({ message: 'File not found' });
    const [response] = formatResponse([file] as unknown as DbResponse[]);
    return res.status(StatusCodes.OK).json(response);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Internal server error' });
  }
};

const deleteFile = async (req: Request, res: Response) => {
  try {
    const gfs = fileStream.gfs();
    const { id } = req.params;
    const { deletedCount } = await gfs.files.deleteOne({ _id: new ObjectId(id) });
    if (!deletedCount) return res.status(StatusCodes.NOT_FOUND).json({ message: 'File not found' });
    return res.status(StatusCodes.NO_CONTENT).end();
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Internal server error' });
  }
};

export default {
  getAllFiles,
  getFileById,
  deleteFile,
};
