import { StatusCodes } from 'http-status-codes';
import { ObjectId } from 'mongodb';
import { Request, Response } from 'express';
import fileStream from '../config/fileStream';

const download = async (req: Request, res: Response) => {
  try {
    const gfs = fileStream.gfs();
    const gridfsBucket = fileStream.gridfsBucket();
    const { id } = req.params;
    const file = await gfs.files.findOne({ _id: new ObjectId(id) });
    if (!file) return res.status(StatusCodes.NOT_FOUND).json({ message: 'File not found' });
    const readStream = gridfsBucket.openDownloadStreamByName(file.filename);
    res.header('Content-Disposition', `attachment; filename="${file.filename.split('-')[1]}"`);
    return readStream.pipe(res);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Internal server error' });
  }
};

export default download;
