import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';

const upload = (req: Request, res: Response) => {
  if (!req.file) return res.status(StatusCodes.BAD_REQUEST).json({ message: 'No file received' });
  return res.status(StatusCodes.CREATED).json(req.file);
};

export default upload;
