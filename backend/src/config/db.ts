import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

const dbConnection = mongoose.createConnection(process.env.MONGO_URL || 'mongodb://localhost:27017/upload');

export default dbConnection;
