import mongoose from 'mongoose';
import createGrid, { Grid } from 'gridfs-stream';
import { GridFSBucket } from 'mongoose/node_modules/mongodb';
import dbConnection from './db';

let gfs: Grid;
let gridfsBucket: GridFSBucket;

dbConnection.once('open', () => {
  gridfsBucket = new mongoose.mongo.GridFSBucket(dbConnection.db, {
    bucketName: 'uploads',
  });
  gfs = createGrid(dbConnection.db, mongoose.mongo);
  gfs.collection('uploads');
});

const mockableGfs = {
  gfs: () => gfs,
  gridfsBucket: () => gridfsBucket,
};

export default mockableGfs;
