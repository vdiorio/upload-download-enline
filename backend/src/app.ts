import express from 'express';
import dotenv from 'dotenv';
import routes from './Routes';

const path = require('path');

const bodyParser = require('body-parser');
const cors = require('cors');

dotenv.config();
const { PORT } = process.env;

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.join(__dirname, '/index')));

app.get('/', (_req, res) => res.sendFile(path.join(__dirname, '/index/index.html')));

app.use('/upload', routes.upload);
app.use('/files', routes.files);
app.use('/download', routes.download);

app.listen(PORT, () => console.log(`Listening on port: ${PORT}`));

export default app;
