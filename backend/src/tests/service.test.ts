import { expect } from 'chai';
import DbResponse from '../interfaces/dbResponse';
import formatResponse from '../services/response.service';

describe('As respostas devem estar prontas para serem consumídas pelo front-enc', () => {
  const unformattedResponse = {
    _id: '629a2b028c46087b0363f9a6',
    length: 410861,
    chunkSize: 261120,
    uploadDate: new Date('2022-06-03T15:38:42.303Z'),
    filename: '1654270722025-Homem lindo.png',
    contentType: 'image/png',
  } as unknown as DbResponse;

  it('A função formatResponse deve receber uma resposta padrão do banco de dados e formatála corretamente', () => {
    const [{
      id, title, size, date,
    }] = formatResponse([unformattedResponse]);
    expect(id).to.be.equal('629a2b028c46087b0363f9a6');
    expect(title).to.be.equal('Homem lindo.png');
    expect(size).to.be.equal(410861);
    expect(date).to.be.equal('05/05/2022');
  });
});
