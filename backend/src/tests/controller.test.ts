import { expect } from 'chai';
import { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import sinon from 'sinon';
import { Readable } from 'stream';
import controllers from '../controllers';
import DbResponse from '../interfaces/dbResponse';
import mockableGfs from '../config/fileStream';

describe('Os controllers da aplicação devem responser corretamente a todas requisições', () => {
  const unformattedProduct = {
    _id: '629a2b028c46087b0363f9a6',
    length: 410861,
    chunkSize: 261120,
    uploadDate: new Date('2022-06-03T15:38:42.303Z'),
    filename: '1654270722025-Homem lindo.png',
    contentType: 'image/png',
  } as unknown as DbResponse;

  const fileMock = {
    fieldname: 'fieldname',
    originalname: 'originalname',
    encoding: 'encoding',
    mimetype: 'mimetype',
    size: 2,
    stream: 'stream' as unknown as Readable,
    destination: 'destination',
    filename: 'filename',
    path: 'path',
    buffer: 'buffer' as unknown as Buffer,
  };
  describe('Controllers de update', async () => {
    let mockReq: Request;
    let mockRes: any;

    before(() => {
      mockReq = {} as Request;
      mockRes = {
        status: (stat: number) => ({
          json: (response: Object) => ({ status: stat, json: response }),
        }),
      };
    });

    it('A rota POST /uploads deve retornar o status 400 se nenhum arquívo for enviado', async () => {
      const response = await controllers.upload(mockReq, mockRes) as any;
      expect(response.status).to.be.equal(StatusCodes.BAD_REQUEST);
      expect(response.json).to.be.deep.equal({ message: 'No file received' });
    });

    it('A rota POST /uploads deve retornar o status 201 e os dados do objeto criado.', async () => {
      mockReq.file = fileMock;
      const response = await controllers.upload(mockReq, mockRes) as any;
      expect(response.status).to.be.equal(StatusCodes.CREATED);
      expect(response.json).to.be.deep.equal(fileMock);
    });
  });

  describe('Controllers de download', () => {
    let mockReq: Request;
    let mockRes: any;

    const stubbedGfs = {
      files: { findOne: async () => unformattedProduct },
    };

    const pipeSpy = sinon.spy();
    const headerSpy = sinon.spy();

    const stubbedBucket = {
      openDownloadStreamByName: () => ({
        pipe: pipeSpy,
      }),
    };

    before(() => {
      mockReq = { params: { id: '629a2b028c46087b0363f9a6' } } as unknown as Request;
      mockRes = {
        header: headerSpy,
        status: (stat: number) => ({
          json: (response: Object) => ({ status: stat, json: response }),
          end: () => ({ status: stat, json: null }),
        }),
      };
      sinon.stub(mockableGfs, 'gfs').returns(stubbedGfs as any);
      sinon.stub(mockableGfs, 'gridfsBucket').returns(stubbedBucket as any);
    });

    after(() => {
      (mockableGfs.gfs as sinon.SinonStub).restore();
      (mockableGfs.gridfsBucket as sinon.SinonStub).restore();
    });

    it('A rota download deve iniciar o download do arquivo com o id requerido', async () => {
      await controllers.download(mockReq, mockRes) as any;
      sinon.assert.calledWith(pipeSpy, mockRes);
      sinon.assert.calledWith(headerSpy, 'Content-Disposition', 'attachment; filename="Homem lindo.png"');
    });

    it('A rota download deve retornar status 404 caso o arquivo não exista', async () => {
      (mockableGfs.gfs as sinon.SinonStub).restore();
      sinon.stub(mockableGfs, 'gfs').returns({
        files: { findOne: async () => null },
      } as any);
      const response = await controllers.download(mockReq, mockRes) as any;
      expect(response.json).to.be.deep.equal({ message: 'File not found' });
      expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
    });
  });

  describe('Controllers de files', async () => {
    let mockReq: Request;
    let mockRes: any;

    before(() => {
      mockReq = { params: { id: '629a2b028c46087b0363f9a6' } } as unknown as Request;
      mockRes = {
        status: (stat: number) => ({
          json: (response: Object) => ({ status: stat, json: response }),
          end: () => ({ status: stat, json: null }),
        }),
      };
    });

    it('A rota /files deve retornar um array com todos os arquivos do banco', async () => {
      sinon.stub(mockableGfs, 'gfs').returns({
        files: {
          find: () => ({ toArray: async () => [unformattedProduct] }),
        },
      } as any);
      const response = await controllers.files.getAllFiles(mockReq, mockRes) as any;
      (mockableGfs.gfs as sinon.SinonStub).restore();
      expect(response.json).to.be.an('array');
      expect(response.status).to.be.equal(StatusCodes.OK);
    });

    it('A rota /files/:id deve retornar um objeto com o id especificado', async () => {
      sinon.stub(mockableGfs, 'gfs').returns({ files: { findOne: async () => unformattedProduct } } as any);
      const response = await controllers.files.getFileById(mockReq, mockRes) as any;
      (mockableGfs.gfs as sinon.SinonStub).restore();
      expect(response.status).to.be.equal(StatusCodes.OK);
      expect(response.json).to.be.an('object');
    });

    it('A rota /files/:id deve retornar status 404 e a mensagem de arquivo não encontrado caso o arquivo não exista', async () => {
      sinon.stub(mockableGfs, 'gfs').returns({ files: { findOne: async () => null } } as any);
      const response = await controllers.files.getFileById(mockReq, mockRes) as any;
      (mockableGfs.gfs as sinon.SinonStub).restore();
      expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
      expect(response.json).to.be.deep.equal({ message: 'File not found' });
    });

    it('A rota Delete /files/:id deve excluir um objeto no banco', async () => {
      sinon.stub(mockableGfs, 'gfs').returns({ files: { deleteOne: async () => ({ deletedCount: 1 }) } } as any);
      const response = await controllers.files.deleteFile(mockReq, mockRes) as any;

      (mockableGfs.gfs as sinon.SinonStub).restore();
      expect(response.status).to.be.equal(StatusCodes.NO_CONTENT);
      expect(response.json).to.be.equal(null);
    });

    it('A rota /files/:id deve retornar status 404 e a mensagem de arquivo não encontrado caso o arquivo não exista', async () => {
      sinon.stub(mockableGfs, 'gfs').returns({ files: { deleteOne: async () => ({ deletedCount: 0 }) } } as any);
      const response = await controllers.files.deleteFile(mockReq, mockRes) as any;
      (mockableGfs.gfs as sinon.SinonStub).restore();
      expect(response.json).to.be.deep.equal({ message: 'File not found' });
      expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
    });
    describe('Testes de integridade', () => {
      before(() => {
        sinon.stub(mockableGfs, 'gfs').throws('Erro');
        mockReq = { params: { id: '629a2b028c46087b0363f9a6' } } as unknown as Request;
        mockRes = {
          status: (stat: number) => ({
            json: (response: Object) => ({ status: stat, json: response }),
            end: () => ({ status: stat, json: null }),
          }),
        };
      });

      after(() => (mockableGfs.gfs as sinon.SinonStub).restore());

      it('Caso ocorra um erro, a rota DELETE /files/:id deve retornar status 500', async () => {
        const response = await controllers.files.deleteFile(mockReq, mockRes) as any;
        expect(response.status).to.be.equal(500);
        expect(response.json).to.be.deep.equal({ message: 'Internal server error' });
      });

      it('Caso ocorra um erro, a rota GET /files/:id deve retornar status 500', async () => {
        const response = await controllers.files.getFileById(mockReq, mockRes) as any;
        expect(response.status).to.be.equal(500);
        expect(response.json).to.be.deep.equal({ message: 'Internal server error' });
      });

      it('Caso ocorra um erro, a rota GET /files deve retornar status 500', async () => {
        const response = await controllers.files.getAllFiles(mockReq, mockRes) as any;
        expect(response.status).to.be.equal(500);
        expect(response.json).to.be.deep.equal({ message: 'Internal server error' });
      });

      it('Caso ocorra um erro, a rota GET /download deve retornar status 500', async () => {
        const response = await controllers.download(mockReq, mockRes) as any;
        expect(response.status).to.be.equal(500);
        expect(response.json).to.be.deep.equal({ message: 'Internal server error' });
      });
    });
  });
});
