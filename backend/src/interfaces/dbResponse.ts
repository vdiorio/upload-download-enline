interface DbResponse {
  _id: string,
  length: number,
  uploadDate: Date,
  filename: string,
  contentType: string
}

export default DbResponse;
