/* eslint-disable no-underscore-dangle */

import DbResponse from '../interfaces/dbResponse';

export default function formatResponse(f: DbResponse[]) {
  const files = [...f];

  return files.map((file) => {
    const year = file.uploadDate.getFullYear();
    const month = file.uploadDate.getMonth().toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
    const day = file.uploadDate.getDay().toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
    return {
      id: file._id,
      title: file.filename.split('-')[1],
      size: file.length,
      date: [day, month, year].join('/'),
      type: file.contentType,
    };
  });
}
