import express from 'express';
import upload from '../model/upload.model';
import controllers from '../controllers';

const router = express.Router();

router.post(
  '/',
  upload.single('file'),
  controllers.upload,
);

export default router;
