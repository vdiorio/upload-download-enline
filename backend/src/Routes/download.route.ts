import express from 'express';
import controllers from '../controllers';

const router = express.Router();

router.get('/:id', controllers.download);

export default router;
