import express from 'express';
import controllers from '../controllers';

const router = express.Router();

router.get('/', controllers.files.getAllFiles);

router.get('/:id', controllers.files.getFileById);

router.delete('/:id', controllers.files.deleteFile);

export default router;
