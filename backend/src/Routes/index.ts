import upload from './upload.route';
import files from './files.route';
import download from './download.route';

export default {
  upload,
  download,
  files,
};
