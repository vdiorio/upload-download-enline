import { GridFsStorage } from 'multer-gridfs-storage';
import dotenv from 'dotenv';

dotenv.config();

const multer = require('multer');

const storage = new GridFsStorage({
  url: process.env.MONGO_URL || 'mongodb://localhost:27017/upload',
  file: (_req, file) => new Promise((resolve) => {
    const filename = `${Date.now()}-${file.originalname}`;
    const fileInfo = {
      filename,
      bucketName: 'uploads',
    };
    resolve(fileInfo);
  }),
});

const upload = multer({ storage });

export default upload;
