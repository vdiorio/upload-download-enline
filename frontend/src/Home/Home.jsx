import React, { useEffect, useState } from 'react';
import ReactLoading from 'react-loading';
import 'react-toastify/dist/ReactToastify.css';
import DropZone from '../DropZone/DropZone';
import FileCard from '../FileCard/FileCard';
import { getFilesInfo } from '../services';

import './Home.css';

export default function Home() {
  const [files, setFiles] = useState([false]);
  const [fetchError, setError] = useState(false);
  const [uploading, setLoad] = useState(false);

  useEffect(() => {
    getFilesInfo().then((res) => setFiles([true, ...res]))
      .catch(() => setError(true));
  }, [uploading]);

  return (
    <>
      <DropZone uploading={uploading} setLoad={setLoad} />
      <h1 className="hero">Faça o download de arquivos incríveis!</h1>
      <div className="card-container" style={files[1] ? {} : { justifyContent: 'center', alignItems: 'center' }}>
        { files[0]
          ? (
            <>
              {files.filter((_f, i) => i).map((file) => <FileCard key={file.id} {...file} />)}
              {!files[1] && (
              <h1>
                Não há arquivos no servidor, quer ser o primeiro a fazer um upload? =)
              </h1>
              )}
            </>
          )
          : (
            <>
              { !fetchError && <ReactLoading type="spin" color="orange" /> }
              { fetchError && (
              <>
                <div className="error-blob">X</div>
                <h1>O servidor não está respondendo, tente novamente.</h1>
              </>
              ) }
            </>
          ) }
      </div>
    </>
  );
}
