import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { download, findFile } from '../services';
import Table from '../Table/Table';

import './DownloadPage.css';

export default function DownloadPage() {
  const { id } = useParams();

  const [file, setFile] = useState({});
  const [time, setTime] = useState(45);

  const downloadLink = download(id);

  const convertSizeUnit = (numberInBytes) => {
    const kb = numberInBytes / 1000;
    if (kb < 1000) return `${Math.floor(kb)}kb`;
    const mb = kb / 1000;
    if (mb < 1000) return `${mb.toFixed(3)}MB`;
    const gb = mb / 1000;
    return `${gb.toFixed(3)}GB`;
  };

  let intervalId;

  useEffect(() => {
    if (time > 0) {
      const timer = window.setInterval(() => {
        setTime(time - 1);
      }, 1000);
      return () => window.clearInterval(timer);
    }
    return undefined;
  }, [time]);

  useEffect(() => {
    if (intervalId) {
      intervalId = setInterval(() => {
        if (time === 45) return clearInterval(intervalId);
        return setTime(time + 1);
      }, 1000);
    }
    findFile(id).then((res) => {
      setFile(res);
    });
  }, []);

  return (
    <>
      <div className="download">
        <div className="info-container">
          <h2>
            Nome do arquivo:
            {' '}
            <span className="file-title">{file.title}</span>
          </h2>
          <h2>
            Descrição:
            {' '}
            <span>{file.id}</span>
          </h2>
          <h2>
            Tamanho do arquivo:
            {' '}
            <span>{convertSizeUnit(file.size)}</span>
          </h2>
        </div>
        <div>
          <h2 className="link">
            Link de download:
            {' '}
            <a className="file-link" href={document.URL}>{document.URL}</a>
          </h2>
        </div>
      </div>
      <div className="page-body">
        <Table />
        <div className="download-links">
          <div className="download-container">
            <a className="button" href={downloadLink}>Premium Download</a>
          </div>
          <div className="download-container">
            { time
              ? <h1>{time}</h1>
              : <a className="button free-download" href={downloadLink}>Free Download</a>}
          </div>
        </div>
      </div>
    </>
  );
}
