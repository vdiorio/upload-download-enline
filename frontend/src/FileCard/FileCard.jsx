import PropTypes from 'prop-types';
import React from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { download } from '../services';
import fileImage from './file.png';

import './FileCard.css';

export default function FileCard({
  title, date, id, type,
}) {
  const image = type.startsWith('image') ? download(id) : fileImage;
  const extention = type.split('/')[1];

  return (
    <a className="card" href={`/download/${id}`}>
      <div className="image-container">
        <img src={image} alt="Avatar" height="100%" />
        { image === fileImage && <p>{`.${extention}`}</p> }
      </div>
      <div className="container">
        <h4><b>{ title }</b></h4>
        <p>
          Criado:
          {' '}
          { date }
        </p>
      </div>
    </a>
  );
}

FileCard.propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
  id: PropTypes.string,
  type: PropTypes.string,
};

FileCard.defaultProps = {
  title: 'Ganhamos.exe',
  date: '01/06/2022',
  id: '111111111',
  type: undefined,
};
