import axios from 'axios';

const backEndHost = process.env.REACT_APP_BACK_HOST || 'http://localhost:3001';

export const upload = async (file, onUploadProgress, signal) => {
  const formData = new FormData();
  formData.append('file', file);
  return axios.request({
    method: 'post',
    url: `${backEndHost}/upload`,
    data: formData,
    onUploadProgress,
    signal,
  });
};

export const download = (id) => `${backEndHost}/download/${id}`;

export const getFilesInfo = async () => fetch(`${backEndHost}/files`).then((res) => res.json());

export const findFile = async (id) => fetch(`${backEndHost}/files/${id}`).then((res) => res.json());

export const orderB = (a, b) => {
  const [aD, aM, aY] = a.split('/');
  const [bD, bM, bY] = b.split('/');
  return (bY - aY) || (bM - aM) || (bD - aD);
};
