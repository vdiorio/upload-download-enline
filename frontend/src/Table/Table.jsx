import React from 'react';
import './Table.css';

export default function Table() {
  return (
    <div className="table-container">
      <table>
        <thead>
          <tr>
            <th>Feature</th>
            <th>Premium</th>
            <th>Free</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              High speed download with
              {' '}
              <span className="file-link">Mega Manager</span>
            </td>
            <td>YES</td>
            <td>NO</td>
          </tr>
          <tr>
            <td>Download speed priority</td>
            <td>Highest</td>
            <td>Lowest</td>
          </tr>
          <tr>
            <td>Maximum paralel downloads</td>
            <td>Unlimited</td>
            <td>1</td>
          </tr>
          <tr>
            <td>Download limit per 24 hours</td>
            <td>Unlimited</td>
            <td>Very limited</td>
          </tr>
          <tr>
            <td>Advertising</td>
            <td>Minimum</td>
            <td>Maximum</td>
          </tr>
          <tr>
            <td>Waiting before each download begins</td>
            <td>None</td>
            <td>45 seconds</td>
          </tr>
          <tr>
            <td>Suport for download acelerators</td>
            <td>YES</td>
            <td>NO</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
