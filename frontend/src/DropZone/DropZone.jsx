import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { buildStyles, CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { upload } from '../services';

export default function DropZone({ uploading, setLoad }) {
  const [dragActive, setDragActive] = useState(false);
  const [progress, setProgress] = useState(0);
  const input = useRef(null);

  const controller = new AbortController();

  const showProgress = (p) => {
    const now = p.loaded / p.total;
    setProgress(now);
  };

  const handleDrop = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (uploading) return false;
    setDragActive(false);
    setLoad(true);
    const [uploadedFile] = e.target.files || e.dataTransfer.files;
    return upload(uploadedFile, showProgress, controller.signal)
      .then(() => toast.success('Seu upload foi um sucesso!'))
      .catch(() => toast.error('Ocorreu um erro durante o upload do arquivo, tente novamente mais tarde.'))
      .finally(() => {
        input.current.value = null;
        setLoad(false);
        setProgress(0);
      });
  };

  const handleDrag = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (e.type === 'dragenter' || e.type === 'dragover') {
      setDragActive(true);
    } else if (e.type === 'dragleave') {
      setDragActive(false);
    }
  };

  return (
    <div className="file-info">
      <ToastContainer />
      <label
        htmlFor="file-input"
        className={dragActive ? 'custom-file-upload file-hover' : 'custom-file-upload'}
        onDragEnter={handleDrag}
        onDragLeave={handleDrag}
        onDragOver={handleDrag}
        onDrop={handleDrop}
      >
        <input ref={input} type="file" id="file-input" onChange={handleDrop} disabled={uploading} />
        {uploading
          ? (
            <div
              style={{ height: '90%', aspectRatio: 1 }}
              role="button"
              tabIndex={0}
              onKeyPress={() => {}}
              onClick={() => (uploading ? controller.abort() : true)}
            >
              <CircularProgressbar
                animated
                value={progress}
                maxValue="1"
                text={`${(progress * 100).toFixed(1)}%`}
                styles={buildStyles({
                  pathColor: `rgba(230, 147, 70, ${progress * 100})`,
                  textColor: '#f88',
                  trailColor: '#d6d6d6',
                  backgroundColor: '#3e98c7',
                })}
              />
            </div>
          )
          : <p>Drop Files here!</p>}
      </label>
    </div>
  );
}

DropZone.propTypes = {
  setLoad: PropTypes.func.isRequired,
  uploading: PropTypes.bool.isRequired,
};
