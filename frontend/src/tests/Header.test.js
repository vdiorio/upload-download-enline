import { render, screen } from '@testing-library/react';
import Header from '../Header'

test('O header possui todos os links de navegação', () => {
  render(<Header />);
  const links = screen.getAllByRole('link');
  ['Home', 'Upload', 'Linkedin', 'Github', 'Repositório'].forEach((link, i) => {
    expect(links[i].textContent).toBe(link);
  });
});

test('Os links direcionam para o lugar correto', () => {
  render(<Header />);
  const links = screen.getAllByRole('link');
  [
    'http://localhost/',
    'http://localhost/',
    'https://www.linkedin.com/in/vitordiorio/',
    'https://github.com/vdiorio',
    'https://gitlab.com/vdiorio/upload-download-enline',
  ].forEach((link, i) => {
    expect(links[i].href).toBe(link);
  });
});

test('O logo está presente no documento', () => {
  render(<Header />);
  expect(screen.getByTestId('logo')).toBeInTheDocument();
  expect(screen.getByTestId('logo-orange')).toBeInTheDocument();
});
