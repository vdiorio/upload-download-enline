import { render, screen } from '@testing-library/react';
import Table from '../Table/Table';

test('A tabela é igual ao do megaupload antigo', () => {
  render(<Table />);
  const thead = screen.getAllByRole('columnheader');
  expect(thead).toHaveLength(3);
  ['Feature', 'Premium', 'Free'].forEach((text, i) => {
    expect(thead[i].textContent).toBe(text)
  });
  expect(screen.getByRole('cell', {
    name: /high speed download with mega manager/i
  })).toBeInTheDocument();
  expect(screen.getByRole('cell', {
    name: /download speed priority/i
  })).toBeInTheDocument();
  expect(screen.getByRole('cell', {
    name: /maximum paralel downloads/i
  })).toBeInTheDocument();
  expect(screen.getByRole('cell', {
    name: /download limit per 24 hours/i
  })).toBeInTheDocument();
  expect(screen.getByRole('cell', {
    name: /advertising/i
  })).toBeInTheDocument();
  expect(screen.getByRole('cell', {
    name: /waiting before each download begins/i
  })).toBeInTheDocument();
  expect(screen.getByRole('cell', {
    name: /suport for download acelerators/i
  })).toBeInTheDocument();  
});
