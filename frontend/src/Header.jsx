import React from 'react';

import './Header.css';

export default function App() {
  return (
    <header>
      <h1 data-testid="logo" className="logo">
        VITOR
        <span data-testid="logo-orange" className="logo-orange">UPLOAD</span>
      </h1>
      <nav>
        <a className="nav-item" href="/">Home</a>
        <a className="nav-item" href="/">Upload</a>
        <a className="nav-item" href="https://www.linkedin.com/in/vitordiorio/">Linkedin</a>
        <a className="nav-item" href="https://github.com/vdiorio">Github</a>
        <a className="nav-item" href="https://gitlab.com/vdiorio/upload-download-enline">Repositório</a>
      </nav>
    </header>
  );
}
